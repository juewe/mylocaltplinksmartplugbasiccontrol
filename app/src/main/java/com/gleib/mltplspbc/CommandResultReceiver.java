package com.gleib.mltplspbc;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by gleib on 10/02/2018.
 */

public class CommandResultReceiver<T> extends ResultReceiver {

    public interface ResultReceiverCallback<T> {
        void onCommandSuccess(T data);
        void onCommandError(Exception exception);
    }

    public static final int RESULT_CODE_OK = 0;
    public static final int RESULT_CODE_ERROR = 1;
    public static final String PARAM_EXCEPTION = "exception";
    public static final String PARAM_RESULT = "result";
    public static final String NAME = "resultreceiver";

    ResultReceiverCallback mReceiver;

    public CommandResultReceiver(Handler handler) {
        super(handler);
    }

    //<editor-fold desc=" Properties ">
    public ResultReceiverCallback getReceiver() {
        return mReceiver;
    }

    public void setReceiver(ResultReceiverCallback mReceiver) {
        this.mReceiver = mReceiver;
    }
    //</editor-fold>

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {

            if (resultCode == RESULT_CODE_OK) {
                mReceiver.onCommandSuccess(resultData.getSerializable(PARAM_RESULT));
            } else {
                mReceiver.onCommandError((Exception) resultData.getSerializable(PARAM_EXCEPTION));
            }
        }
    }
}

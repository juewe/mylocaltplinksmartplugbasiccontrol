package com.gleib.mltplspbc;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;


/**
 * Created by gleib on 14/01/2018.
 */

public class CommandIntentService extends IntentService {

    public CommandIntentService() {
        super("CommandIntentService");
    }

    protected String sendCommand(String command, String host) throws IOException {

        Socket socket = new Socket(host, 9999);
        OutputStream outputStream = socket.getOutputStream();
        outputStream.write(encryptWithHeader(command));

        InputStream inputStream = socket.getInputStream();
        String data = decrypt(inputStream);

        outputStream.close();
        inputStream.close();
        socket.close();

        return data;
    }
    private String decrypt(InputStream inputStream) throws IOException {

        int in;
        int key = 0x2B;
        int nextKey;
        StringBuilder sb = new StringBuilder();
        in = inputStream.read();
        while(inputStream.available() > 0) {

            nextKey = in;
            in = in ^ key;
            key = nextKey;
            sb.append((char) in);
            in = inputStream.read();
        }
        if (sb.length() > 0)
            return "{" + sb.toString().substring(5);
        return "";
    }

    private int[] encrypt(String command) {

        int[] buffer = new int[command.length()];
        int key = 0xAB;
        for(int i = 0; i < command.length(); i++) {

            buffer[i] = command.charAt(i) ^ key;
            key = buffer[i];
        }
        return buffer;
    }

    private byte[] encryptWithHeader(String command) {

        int[] data = encrypt(command);
        byte[] bufferHeader = ByteBuffer.allocate(4).putInt(command.length()).array();
        ByteBuffer byteBuffer = ByteBuffer.allocate(bufferHeader.length + data.length).put(bufferHeader);
        for(int in : data) {

            byteBuffer.put((byte) in);
        }
        return byteBuffer.array();
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {


        Bundle b = intent.getBundleExtra("EXTRABUNDLE");
        Device dev = (Device) b.getSerializable("DEVICE");
        String cmd = b.getString("COMMAND");

        System.out.printf("SENDING TO %s - %s%n", dev.name, dev.address);

        ResultReceiver rr = intent.getParcelableExtra(CommandResultReceiver.NAME);

        try {

            String res = sendCommand(cmd, dev.address);

            System.out.printf("DATA RETURNED: %s%n", res);

            b = new Bundle();
            b.putSerializable(CommandResultReceiver.PARAM_RESULT, res);

            rr.send(CommandResultReceiver.RESULT_CODE_OK, b);

        } catch (IOException e) {

            b = new Bundle();
            b.putSerializable(CommandResultReceiver.PARAM_EXCEPTION, e);

            rr.send(CommandResultReceiver.RESULT_CODE_ERROR, b);
        }

    }
}

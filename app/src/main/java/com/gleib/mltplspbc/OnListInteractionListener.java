package com.gleib.mltplspbc;

/**
 * Created by gleib on 14/01/2018.
 */

public interface OnListInteractionListener {
    void onListInteraction(Device item, Integer position);
}
